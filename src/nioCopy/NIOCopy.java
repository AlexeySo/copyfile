package nioCopy;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;

public class NIOCopy extends Thread {
 public String[] args;
 private int index;

  public NIOCopy(String[] args,int index){
      this.args = args;
      this.index = index;
     }
    public void  run(){

        if(args.length!=3){
            System.out.println("Введите нужное количество путей");
            return;
        }
        Long startTime = System.currentTimeMillis();
        System.out.println("Копирование из "+args[0]+" в "+args[index]);
        Path input  =Paths.get(args[0]) ;
        Path target = Paths.get(args[index]);
        try {
               Files.copy(input,target, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Long endTime = System.currentTimeMillis() - startTime;
        System.out.println("Время работы "+getName()+"а: "+ endTime);

    }
}
