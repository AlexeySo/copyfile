package nioCopy;

public class NIOParallelCopy {
    public static void main(String[] args) throws InterruptedException {
        args[0]="D:\\input.txt";
        args[1]="D:\\out.txt";
        args[2]="D:\\out1.txt";
        Thread copyFile = new NIOCopy(args,1);
        Thread copyFile1 = new NIOCopy(args,2);
        Long startTime = System.currentTimeMillis();
        copyFile.start();
        copyFile1.start();
        copyFile.join();
        copyFile1.join();
        Long endTime = System.currentTimeMillis() - startTime;
        System.out.println("Время работы потоков: "+ endTime);

    }
}
