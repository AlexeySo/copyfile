package nioCopy;

public class NIOSequentialCopy {
    public static void main(String[] args){
        Thread copyFile = new NIOCopy(args,1);
        Thread copyFile1 = new NIOCopy(args,2);
        Long startTime = System.currentTimeMillis();
        copyFile.run();
        copyFile1.run();
        Long endTime = System.currentTimeMillis() - startTime;
        System.out.println("Время работы потоков: "+ endTime);

    }
}
