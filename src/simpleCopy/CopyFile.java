package simpleCopy;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class CopyFile extends Thread  {
    private String inputPathFile;
    private String outputPathFile;

    public CopyFile(String name,String inputPathFile, String outputPathFile) {
       this.inputPathFile = inputPathFile;
       this.outputPathFile = outputPathFile;
       setName(name);
    }

    public void run(){
        Long startTime = System.currentTimeMillis();
        System.out.println("Копирование из "+inputPathFile+" в "+outputPathFile);
        List<String> list = null;
        try {
            list = Files.readAllLines(Paths.get(inputPathFile));
            Files.write(Paths.get(outputPathFile), list);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Long endTime = System.currentTimeMillis() - startTime;
        System.out.println("Время работы "+getName()+"а: "+ endTime);
       }
}