package simpleCopy;

public class Sequential {

    public static void main(String[] args) throws InterruptedException {
            Thread copyFiles = new CopyFile("1 поток", "D:\\input.txt", "D:\\output.txt");
            Thread copyFiles1 = new CopyFile("2 поток", "D:\\input1.txt", "D:\\output1.txt");
               int startTime = (int) System.currentTimeMillis();
                copyFiles.run();
                copyFiles1.run();
                int totalTime = (int) (System.currentTimeMillis() - startTime);
            System.out.println("Общее время копирования файлов последовательно: " + totalTime);
        }

    }

