package simpleCopy;

public class Parallel {
    public static void main(String[] args) throws InterruptedException {
        Thread copyFiles = new CopyFile("1 поток", "D:\\input.txt", "D:\\output.txt");
        Thread copyFiles1 = new CopyFile("2 поток", "D:\\input1.txt", "D:\\output1.txt");
        Long startTime = System.currentTimeMillis();
        copyFiles1.start();
        copyFiles.start();
        copyFiles.join();
        copyFiles1.join();
        long totalTime = System.currentTimeMillis() - startTime;
        System.out.println("Общее время копирования файлов парраллельно: " + totalTime);

    }
}

